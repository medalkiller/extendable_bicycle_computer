// ---------------- General  Includes -------------------------- //
#include <Arduino.h>
#include <SPI.h>
#include <Wire.h>
//#include <Time.h>
//#include "libraries/SPI/SPI.h"
//#include "libraries/Wire/Wire.h"
//
#include <Time.h>

// ----------------------- Internal variables ----------------- //
volatile uint64_t nbWheelLoop = 0;

// ------------------------ Parameters ------------------------ //
float wheelPerimeter = 2.0;        //m

// --------------------- Hall sensor  --------------------------- //
#define HALL_SENSOR_PIN 2

// ---------------- OLED Display SSD1306 ------------------ //
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);

#define NUMFLAKES 10
#define XPOS 0
#define YPOS 1
#define DELTAY 2

// ------------------------- RTC ---------------------------------- //
#include <DS1307RTC.h>
//#include <TimeLib.h>
tmElements_t tm;
DS1307RTC myRTC;


// ------------------------- SD ---------------------------------- //
#include <SdFat.h>

const uint8_t chipSelect = 10; // SD chip select pin.  Be sure to disable any other SPI devices such as Enet.
// Log file base name.  Must be six characters or less.
#define FILE_BASE_NAME "Data"
SdFat sd;
SdFile file;
// Time in micros for next data record.
volatile uint32_t logTime;

void writeHeader() {
  file.println("Year, Month, Day, Hour, Minute, Second, Distance (m)");
  file.flush();
}

// Log a data record.
void logData() {
    // Write data to file.  Start with log time in micros.
    file.print(tm.Year + 1970);
    file.write(',');
    file.print(tm.Month);
    file.write(',');
    file.print(tm.Day);
    file.write(',');
    file.print(tm.Hour);
    file.write(',');
    file.print(tm.Minute);
    file.write(',');
    file.print(tm.Second);
    file.write(',');
    file.print((double)nbWheelLoop*wheelPerimeter);
    file.println();
    file.flush();
}
// Error messages stored in flash.
#define error(msg) sd.errorHalt(F(msg))

// --------------------------- ISR ----------------------------------- //
void newWheelLoop()
{
    nbWheelLoop++;
    sei();
    myRTC.read(tm);
    cli();
}

// -------------------- SETUP ------------------------------------- //
void setup() {
    Serial.begin(9600);
    // Wait for char input before setup ...
    Serial.println(F("Serial initialized : Waiting for input ..."));
    while (!Serial.available()) {
    //    SysCall::yield();
      }
/********* Hall sensor  *****************************/
    pinMode(HALL_SENSOR_PIN, INPUT);
    attachInterrupt(digitalPinToInterrupt(HALL_SENSOR_PIN), newWheelLoop, RISING);


/********* OLED DISPLAY**************************/
    display.begin(SSD1306_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3C (for the 128x32)
    // Init OLED display and display init message
    display.clearDisplay();
    display.setTextSize(1);
    display.setTextColor(WHITE);
    display.setCursor(0,0);
    display.println("Display OK");
    display.display();
    delay(2000);
    display.clearDisplay();

/************* RTC ********************************/
    if (myRTC.read(tm)) {
        Serial.print("Ok, Time = ");
        Serial.print(tm.Year);
      } else {
        if (myRTC.chipPresent()) {
          Serial.println("The DS1307 is stopped.  Please run the SetTime");
          Serial.println("example to initialize the time and begin running.");
          Serial.println();
        } else {
          Serial.println("DS1307 read error!  Please check the circuitry.");
          Serial.println();
        }
     }

///************* SD *********************************/
    // Create uniq filename
  const uint8_t BASE_NAME_SIZE = sizeof(FILE_BASE_NAME) - 1;
  char fileName[13] = FILE_BASE_NAME "00.csv";
  Serial.println(F("Type any character to start"));
  while (!Serial.available()) {
    //SysCall::yield();
  }
  // Initialize the SD card at SPI_HALF_SPEED to avoid bus errors with
  // breadboards.  use SPI_FULL_SPEED for better performance.
  if (!sd.begin(chipSelect, SPI_HALF_SPEED)) {
    //sd.initErrorHalt();
    Serial.println("Error chid select");
  }
  Serial.println("SD Begin OK ");

  // Find an unused file name.
  if (BASE_NAME_SIZE > 6) {
    error("FILE_BASE_NAME too long");
    Serial.println("FILE_BASE_NAME too long");
  }
  while (sd.exists(fileName)) {
    if (fileName[BASE_NAME_SIZE + 1] != '9') {
      fileName[BASE_NAME_SIZE + 1]++;
    } else if (fileName[BASE_NAME_SIZE] != '9') {
      fileName[BASE_NAME_SIZE + 1] = '0';
      fileName[BASE_NAME_SIZE]++;
    } else {
      error("Can't create file name");
      Serial.println("Can't create file name");
    }
  }
  Serial.println("File does not exist OK");
  if (!file.open(fileName, O_CREAT | O_WRITE | O_EXCL)) {
    error("file.open");
    Serial.println("file.open");
  }
  Serial.println("File open OK");
  Serial.print(F("Logging to: "));
  Serial.println(fileName);
  Serial.println(F("Type any character to stop"));

  writeHeader();
  Serial.println("Header written ");
  if (!file.sync() || file.getWriteError()) {
    error("write error");
  }
}

// -------------------- Internal Functions ------------------------- //
//void displayDistance(double distance)
//{
//    display.clearDisplay();
//    display.setTextSize(1);
//    display.setCursor(0,0);
//    display.print("Distance : ");
//    display.setCursor(0,15);
//    display.setTextSize(2);
//    display.print((double)distance);
//    display.setTextSize(1);
//    display.setCursor(100,15);
//    display.print("km");
//    display.display();
//}

// -------------------------- LOOP ---------------------------------- //
void loop() {
  //digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)
 // displayDistance((double)nbWheelLoop*0.315);
 // delay(500);              // wait for a second
 // digitalWrite(13, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);              // wait for a second
// displayDistance((double)nbWheelLoop*0.315);
 logData();
 Serial.println("Data logged ! OK ");
 Serial.println("NbWheelLoop : ");
 Serial.println((double)nbWheelLoop*wheelPerimeter);
}

