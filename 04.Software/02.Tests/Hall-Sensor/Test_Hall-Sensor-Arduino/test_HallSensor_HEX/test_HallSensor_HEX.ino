// constants won't change. They're used here to set pin numbers:
const int hallPin = 12;     // the number of the hall effect sensor pin
const int ledPin =  13;     // the number of the LED pin
const int hallPin2 = 11;
// variables will change:
int hallStateA0 = 0;          // variable for reading the hall sensor status
int hallStateD0 = 0;


void setup() {
  // initialize the LED pin as an output:
  pinMode(ledPin, OUTPUT);      
  // initialize the hall effect sensor pin as an input:
  pinMode(hallPin, INPUT);   
  pinMode(hallPin2, INPUT);   
    Serial.begin(9600);
  
}

void loop(){
  // read the state of the hall effect sensor:
  hallStateD0 = digitalRead(hallPin);
  hallStateA0 = analogRead(hallPin2);
      Serial.print("\n Voltage D0: ");
      Serial.print(hallStateD0);
      Serial.print("|  Voltage A0: ");
      Serial.print(hallStateA0);
      Serial.println("------------------");

//
//  if (hallState == LOW) {     
//    // turn LED on:    
//    digitalWrite(ledPin, HIGH);  
//  } 
//  else {
//    // turn LED off:
//    digitalWrite(ledPin, LOW); 
//  }
  delay(200);
}
