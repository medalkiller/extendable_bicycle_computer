---
title: Cahier des charges *Compteur de vélo*
author:
- name: Julien Baumeyer
  email: docteurjojo3@gmail.com
- name: Anne-Laure Huet
  email: a.l.huet@gmail.com
date: septembre 2016
abstract: Résumé du cahier des charges du projet compteur vélo 
...

# Fonctions 

- Affiche les informations sur un ecran en temps-reel
- Enregistrer les données sur SD 
- Données brutes ASCII non binaires 
- Possibilité d'eteindre / allumer (alimentation)
- Fonctionne sans secteur -->  autonome(alimentation)
- Transfert sans fil des données (téléphonee)
- Se recharge avec un chargeur téléphone classique
- Se recharge en roulant si on veut (dynamo ? )
- Possibilité de changer la batterie (btterie amovible)
- Donne le niveau de la batterie (pourcentage ou durée restante ?? ) 
- Enregistre le vrai jour et heure/min/sec
- Enregistrer des fichiers a chaque allumage ?
- Resiste à la pluie 
- Resiste aux rayures 
- Facilement déplacable sur des velo (sauf les capteurs ?)
- Fonctione en plein soleil
- Enregistrement de la position GPS 
- Enregestrisement de l'altitude parcourue 
- Autonomie > 10 h 
- Justesse > 5% distance (et les autres ? ) 
- Affichage fluide (> 5Hz) 
- Voyant de test allume a chaque passage roue (si Debug activé )
- Plusieurs mode d'affichage 
- Affichage numérique | Compteur de voiture | autres ? 
- Petit et leger sur le guidon (Taille max = AxAxA mm) 
- Bien intégré ce qui est visible | esthétique | petit 
- 


- Recharge en roulant par dynamo


- Diametre de roue paramétrable 

## Caractéristiques mécaniques 


## Affichage 
- Zone informatios toujours présente (batterie, heure, températeur, témoins tout va bien ? connexion capteurs ? etc.)
- Zone changeante (soit distance totale, vitesse actuelle, etc.)
    - Affiche les infos possible (correspondant aux options qui sont présentes mais pas les autres) 
- Changement zone changeante par appui sur bouton 1 position
- 

## Caractéristiques techniques
### Affichage 
- Resistant aux rayures 
- Resistant à la pluie 
- Fluide (<200ms)

### Alimentation
- Batterie amovible 
- Rechargement par chargeur téléphone 

## Debug 
- Mode debug 
    - LED à chaque passage roue

## Enregistrement
- Horodatée
- ASCII 



## Fonctionnement 
- Diagramme pieuvre ? Bloc ? 
1. Remise à zéro :
    - Appui > 5s sur bouton RAZ -> nouvel Enregistrement (toutes les moyennes sont remises à 0)


## Modules (options)
- Capteur roue
    - Distance parcourue
    - Vitesse instantanée
    - Vitesse moyenne
- GPS (trace, altitude) 
- Baromètre (MPL3115A2)
- Bluetooth (téléphone) 
- Dynamo (recharge ne roulant) 
- Thermomètre (Température )
- Hygrométrie ?

## Mesures 
- Distance parcourue depuis allumage 
- Vitesse instantanée 
- Viesse moyenne depuis allumage
- Vitesse max 
- Accélération ? 
- Position GPS 
- Dénivelé moyenne 
- Dénivelé + 
- Dénivelé - 
- Altitude instantanée
- Température instantanée
- Température max / min 
- Hygrométrie instantanée

## Gestion des erreurs 

## Paramètres 
### Module aimant roue 
- Diametre roue 

### Ecran
- Frequence rafraichissement

### Thermomètre
- Frequence rafraichissement

### GPS
- Frequence refraichissement

### Bluetooth 
- Freq serie 
- Nom
- PIN 
-    --> Calibration au début ?? 
- Perform calibration (yes or no)
- 

