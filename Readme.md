# Project overview

This project aims at developping a module-base cycling computer.
The core functions are based on Arduino boards, several types can be used (Mega, Pro-Mini, Nano).
The main function provide :
    - total distance computation
    - csv timestamped logging (to micro sd memory card)
    - 0.96" screen display 
    - Current time
    - Json-based configuration file
Each additionnal function such as altimer, GPS, temperature, Bluetooth and so-on could be added and integrated in the core software as long as backward compatibilty is garantee.
Then, parameters can be added to main configuratuioon fil 'config.json'.

# Mechanical design 

THe mechanical design is based on : 
    - handle-bar screen fixation
    - wheel sensor fixation 
    
Those parts can be 3D-printed. 

# Software design 

Developped using Arduino IDE *v1.8.5*.

## Libraries

Depend upon the following libraries :
    - SPI
    - Wire
    - Time
    - Adafruit_GFX
    - Adafruit_SSD1306
    - DS1307RTC
    - SdFat 
